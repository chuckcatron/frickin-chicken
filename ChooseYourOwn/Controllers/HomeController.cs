﻿using System.Web.Mvc;

namespace ChooseYourOwn.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}